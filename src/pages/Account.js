import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Dialog } from '@headlessui/react'
import { UserAddIcon } from '@heroicons/react/outline'
import Navbar from '../components/Navbar'
import Modal from '../components/common/Modal'

const Account = () => {

    const [ isLoading, setIsLoading ] = useState(false)
    const [ datas, setDatas ] = useState([])
    const [ fullName, setFullName ] = useState('#DB#')
    const [ isOpen, setIsOpen ] = useState(false)

    useEffect(() => {
        setIsLoading(true)

        getAccounts()
    }, [])

    const getAccounts = () => {
        axios.post('https://front-end-test-assignment.fintech-market.com/api/v1/accounts/search', {
            data: {
                name: '#DB#'
            }
        }, {
            headers: {
                Authorization: 'Basic ZGFuYWJpamFrOmlWcFprMUMwQUZGd0RmRFY='
            }
        }).then((res) => {
            setDatas(res.data.data)
            setIsLoading(false)
        }).catch(() => {
            setIsLoading(false)
        })
    }

    const createAccountForm = () => {
        setFullName('')
        setIsOpen(true)
        // clear form
    }

    const createAccount = () => {
        if(fullName === '') {
            alert('Input your fullname')
            return
        }
        setIsLoading(true)
        axios.post('https://front-end-test-assignment.fintech-market.com/api/v1/accounts', {
            data: {
                name: `#DB# ${fullName}`
            }
        }, {
            headers: {
                Authorization: 'Basic ZGFuYWJpamFrOmlWcFprMUMwQUZGd0RmRFY='
            }
        }).then((res) => {
            setIsOpen(false)
            getAccounts()
        }).catch(() => {
            setIsLoading(false)
        })
    }

    return (
        <>
            <Navbar />
            <section className="antialiased bg-gray-100 text-gray-600 h-screen px-4">
                <div className="flex flex-col pt-10 h-full">
                    <div className="w-full max-w-2xl mx-auto bg-white shadow-lg rounded-sm border border-gray-200">
                        <div className="grid grid-cols-2 gap-2">
                            <div>
                                <header className="px-5 py-4 border-b border-gray-100">
                                    <h2 className="font-semibold text-gray-800">Accounts </h2>
                                </header>
                            </div>
                            <div className="relative">
                                <div className="absolute top-0 right-0 m-2">
                                    <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded" onClick={createAccountForm}>
                                        Create Account
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                        <div className="p-3">
                            <div className="overflow-x-auto">
                                <table className="table-auto w-full">
                                    <thead className="text-xs font-semibold uppercase text-gray-400 bg-gray-50">
                                        <tr>
                                            <th className="p-2 whitespace-nowrap">
                                                <div className="font-semibold text-left">Account ID</div>
                                            </th>
                                            <th className="p-2 whitespace-nowrap">
                                                <div className="font-semibold text-left">Name</div>
                                            </th>
                                            <th className="p-2 whitespace-nowrap">
                                                <div className="font-semibold text-left">Balance</div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody className="text-sm divide-y divide-gray-100">
                                        {
                                            datas.length > 0 ?
                                            datas.map((account) => {
                                                return (
                                                    <tr key={account.id}>
                                                        <td className="p-2 whitespace-nowrap">
                                                            <div className="text-left">{account.id}</div>
                                                        </td>
                                                        <td className="p-2 whitespace-nowrap">
                                                            <div className="text-left">{account.name}</div>
                                                        </td>
                                                        <td className="p-2 whitespace-nowrap">
                                                            <div className="text-left font-medium text-green-500">IDR{account.balance}</div>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                            :
                                            <tr>
                                                <th colSpan={3}>No Results</th>
                                            </tr>
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Modal setIsOpen={setIsOpen} isOpen={isOpen}>
                <div className="relative inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                    <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div className="sm:flex sm:items-start">
                            <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-green-100 sm:mx-0 sm:h-10 sm:w-10">
                                <UserAddIcon className="h-6 w-6 text-green-600" aria-hidden="true" />
                            </div>
                            <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900">
                                    Create Account
                                </Dialog.Title>
                                <div className="mt-2">
                                    <div className="mb-4">
                                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="name" type="text" placeholder="Name"
                                        value={fullName} 
                                        onChange={e => setFullName(e.target.value)} 
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                        <button
                            type="button"
                            className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                            onClick={() => setIsOpen(false)}
                        >
                            Cancel
                        </button>
                        <button
                            type="button"
                            className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm"
                            onClick={createAccount}
                            disabled={isLoading}
                        >
                            Create
                        </button>
                    </div>
                </div>
            </Modal>
        </>
    );
};

export default Account;
