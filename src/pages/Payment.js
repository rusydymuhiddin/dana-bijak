import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Navbar from '../components/Navbar'
import Modal from '../components/common/Modal'

const Payment = () => {
    const [ isLoading, setIsLoading ] = useState(false)
    const [ datas, setDatas ] = useState([])
    const [ remitters, setRemitters ] = useState([])
    const [ beneficiaries, setBeneficiaries ] = useState([])
    const [ isOpen, setIsOpen ] = useState(false)

    // form here
    const [ paymentType, setPaymentType ] = useState([]);
    const [ paymentAmount, setPaymentAmount ] = useState(5000);
    const [ paymentDescription, setPaymentDescription ] = useState('Urgent Needs');
    const [ paymentRemitter, setPaymentRemitter ] = useState('');
    const [ paymentBeneficiary, setPaymentBeneficiary ] = useState('');

    useEffect(() => {
        setIsLoading(true)

        getPayments()
        getAccounts()
    }, [])

    const getPayments = () => {
        axios.post('https://front-end-test-assignment.fintech-market.com/api/v1/payments/search', {
            data: {}
        }, {
            headers: {
                Authorization: 'Basic ZGFuYWJpamFrOmlWcFprMUMwQUZGd0RmRFY='
            }
        }).then((res) => {
            setDatas(res.data.data)
            setIsLoading(false)
        }).catch(() => {
            setIsLoading(false)
        })
    }

    const getAccounts = () => {
        axios.post('https://front-end-test-assignment.fintech-market.com/api/v1/accounts/search', {
            data: {
                name: "#DB#"
            }
        }, {
            headers: {
                Authorization: 'Basic ZGFuYWJpamFrOmlWcFprMUMwQUZGd0RmRFY='
            }
        }).then((res) => {
            setRemitters(res.data.data)
            setBeneficiaries(res.data.data)
            setIsLoading(false)
        }).catch(() => {
            setIsLoading(false)
        })
    }

    const createPaymentForm = () => {
        setIsOpen(true)
        // clear form
    }

    const getAccountRemitter = (field) => {
        const remitter = remitters.find((r) => r.id === paymentRemitter);

        return remitter[field]
    }

    const getAccountBeneficiary = (field) => {
        const beneficiary = beneficiaries.find((r) => r.id === paymentBeneficiary);

        return beneficiary[field]
    }

    const getIncomingPayload = () => {
        return {
            description: paymentDescription,
            amount: paymentAmount,
            type_key: paymentType,
            beneficiary_account_id: getAccountBeneficiary('id'),
            remitter_name: getAccountRemitter('name'),
            remitter_account_number: getAccountRemitter('number'),
        }
    }

    const getInternalPayload = () => {
        return {
            description: paymentDescription,
            amount: paymentAmount,
            type_key: paymentType,
            beneficiary_account_id: getAccountBeneficiary('id'),
            remitter_account_id: getAccountRemitter('id'),
        }
    }

    const getOutgoingPayload = () => {
        return {
            description: paymentDescription,
            amount: paymentAmount,
            type_key: paymentType,
            remitter_account_id: getAccountRemitter('id'),
            beneficiary_name: getAccountBeneficiary('name'),
            beneficiary_account_number: getAccountRemitter('number'),
        }
    }

    const createPayment = () => {

        if (paymentType === '' || paymentAmount < 0 || paymentDescription === '' || paymentBeneficiary === '' || paymentRemitter === '') {
            alert('All form should be filled')

            return;
        }
        
        let payload = {}

        if(paymentType === 'incoming') {
            payload = getIncomingPayload()
        } else if(paymentType === 'internal') {
            payload = getInternalPayload()
        } else {
            payload = getOutgoingPayload()
        }

        setIsLoading(true)

        axios.post('https://front-end-test-assignment.fintech-market.com/api/v1/payments', {
            data: payload
        }, {
            headers: {
                Authorization: 'Basic ZGFuYWJpamFrOmlWcFprMUMwQUZGd0RmRFY='
            }
        }).then((res) => {
            setIsOpen(false)
            getPayments()
            getAccounts()
        }).catch(() => {
            setIsLoading(false)
        })
    }

    const handleSelectPaymentType = (event) => {
        setPaymentType(event.target.value)
    }

    const handleSelectPaymentRemitter = (event) => {
        setPaymentRemitter(event.target.value)
    }
    const handleSelectPaymentBeneficiary = (event) => {
        setPaymentBeneficiary(event.target.value)
    }

    return (
        <>
            <Navbar />
            <section className="antialiased bg-gray-100 text-gray-600 h-screen px-4">
                <div className="flex flex-col pt-10 h-full">
                    <div className="w-full mx-auto bg-white shadow-lg rounded-sm border border-gray-200">
                        <div className="grid grid-cols-2 gap-2">
                            <div>
                                <header className="px-5 py-4 border-b border-gray-100">
                                    <h2 className="font-semibold text-gray-800">Payments </h2>
                                </header>
                            </div>
                            <div className="relative">
                                <div className="absolute top-0 right-0 m-2">
                                    <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded" onClick={createPaymentForm}>
                                        Create Payment
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                        <div className="p-3">
                            <div className="overflow-x-auto">
                                <table className="table-auto w-full">
                                    <thead className="text-xs font-semibold uppercase text-gray-400 bg-gray-50">
                                        <tr>
                                            <th className="p-2 whitespace-nowrap">
                                                <div className="font-semibold text-left">Payment Type</div>
                                            </th>
                                            <th className="p-2 whitespace-nowrap">
                                                <div className="font-semibold text-left">Remitter</div>
                                            </th>
                                            <th className="p-2 whitespace-nowrap">
                                                <div className="font-semibold text-left">Beneficiary</div>
                                            </th>
                                            <th className="p-2 whitespace-nowrap">
                                                <div className="font-semibold text-left">Description</div>
                                            </th>
                                            <th className="p-2 whitespace-nowrap">
                                                <div className="font-semibold text-left">Amount</div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody className="text-sm divide-y divide-gray-100">
                                        {
                                            datas.length > 0 ?
                                            datas.map((payment) => {
                                                return (
                                                    <tr key={payment.id}>
                                                        <td className="p-2 whitespace-nowrap">
                                                            <div className="text-left">{payment.type_key}</div>
                                                        </td>
                                                        <td className="p-2 whitespace-nowrap">
                                                            <div className="text-left">{payment.remitter_name} ({payment.remitter_account_number})</div>
                                                        </td>
                                                        <td className="p-2 whitespace-nowrap">
                                                            <div className="text-left">{payment.beneficiary_name} ({payment.beneficiary_account_number})</div>
                                                        </td>
                                                        <td className="p-2 whitespace-nowrap">
                                                            <div className="text-left">{payment.description}</div>
                                                        </td>
                                                        <td className="p-2 whitespace-nowrap">
                                                            <div className="text-left font-medium text-green-500">IDR{payment.amount}</div>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                            :
                                            null
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Modal setIsOpen={setIsOpen} isOpen={isOpen}>
                <div className="relative inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                    <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div className="sm:flex sm:items-start">
                            <div className="flex justify-center">
                                <div className="mb-3 xl:w-96">
                                    <h1 className="font-medium mb-3 text-right">Create New Payment</h1>
                                    <hr className='mb-3' />
                                    <label className="font-medium">Select Payment Type</label>
                                    <select value={paymentType} onChange={handleSelectPaymentType} className="form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" aria-label="Default select example">
                                        <option value="" key="pt-0">Select Payment Type</option>
                                        <option value="incoming" key="incoming">Incoming</option>
                                        <option value="internal" key="internal">Internal</option>
                                        <option value="outgoing" key="outgoing">Outgoing</option>
                                    </select>
                                    
                                    <label className="font-medium">Input Amount</label>
                                    <input value={paymentAmount} onChange={e => setPaymentAmount(e.target.value)} className="mt-2 shadow appearance-none border rounded mt-2 w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="Amount" type="number" placeholder="Amount" />

                                    <label className="font-medium">Input Description</label>
                                    <textarea value={paymentDescription} onChange={e => setPaymentDescription(e.target.value)} className="mt-2 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="Description"placeholder="Description"></textarea>
                                    
                                    <label className="font-medium">Select Remitter</label>
                                    <select value={paymentRemitter} onChange={handleSelectPaymentRemitter} id="remitter" className="mt-2 form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" aria-label="Default select example">
                                        <option value="" key="r0">Select Remitter</option>
                                        {remitters && remitters.map((remitter) => {
                                            return (
                                                <option value={remitter.id} key={remitter.id}>{remitter.name} - Balance ({remitter.balance})</option>
                                            )
                                        })}
                                    </select>

                                    <label className="font-medium">Select Beneficiary</label>
                                    <select value={paymentBeneficiary} onChange={handleSelectPaymentBeneficiary} id="beneficiary" className="mt-2 form-select appearance-none block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" aria-label="Default select example">
                                        <option value="" key="b0">Select Beneficiary</option>
                                        {beneficiaries && beneficiaries.map((beneficiary) => {
                                            return (
                                                <option value={beneficiary.id} key={beneficiary.id}>{beneficiary.name} - Balance ({beneficiary.balance})</option>
                                            )
                                        })}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                        <button
                            type="button"
                            className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                            onClick={() => setIsOpen(false)}
                        >
                            Cancel
                        </button>
                        <button
                            type="button"
                            className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm"
                            onClick={createPayment}
                            disabled={isLoading}
                        >
                            Create
                        </button>
                    </div>
                </div>
            </Modal>
        </>
    );
};

export default Payment;
