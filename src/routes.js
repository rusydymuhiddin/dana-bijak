import Home from "./pages/Home";
import NotFound from "./pages/NotFound";
import Account from "./pages/Account";
import Payment from "./pages/Payment";

export const routes = [
    {path: "/", element: <Home/>},
    {path: "*", element: <NotFound/>},
    {path: "/account", element: <Account/>},
    {path: "/payment", element: <Payment/>},
]
